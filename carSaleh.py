import weakref

class car:
	def __init__(self, prev_car=None, min_buffer=0.1):
		"""
		car class constructor. This gets called when we create a car object.

		arguments:
		prev_car - the car in front of this car
		min_buffer - the minimum space between this car and the one in front

		returns nothing
		"""
		self.velocity=1.0
		self.position=0.0
		self.min_buffer=0.1
		if prev_car!=None:
			self.prev_car=weakref.ref(prev_car)
			self.prev_car().add_next(self)
		else:
			self.prev_car = None
		self.next_car=None

	def __del__(self):
		"""
		destructor. Called when we call 'del' on the car. Makes it so the previous
		car knows it is not there anymore.

		returns nothing
		"""
		if self.next_car!=None:
			try:
				self.next_car().remove_prev()
			except AttributeError:
				pass

	def update(self, dt, scaling):
		"""
		checks the car in front of it has slowed down. If it has, it will also slow down.

		arguments:
		dt - timestep. How much time elapses while we move the car?

		returns nothing
		"""
		self.scale=scaling
		if self.prev_car!=None and self.prev_car().get_pos()-self.position<=1:
			self.accelerate(self.prev_car().get_pos(),self.prev_car().get_vel(),dt)
		else:
			self.accelerate(self.position+1,1.0,dt)
		self.position += self.velocity*dt

	def accelerate(self, obj_pos, obj_vel, dt):
		"""
		accelerates the object and changes its velocity

		arguments:
		obj_pos - the position of the object our car will base its acceleration from
		obj_vel - the velocity of the object our car will base its acceleration from
		dt - timestep our car accelerates for

		returns nothing
		"""
		d = obj_pos - self.position - self.min_buffer
		if d == 0:
			a = 0.0
		elif d < 0:
			d*=-1
			v_f = obj_vel if obj_vel<self.velocity else self.velocity
			v_i = obj_vel if obj_vel>=self.velocity else self.velocity
			a = ((v_f**2)-(v_i**2))/(2.0*d)
		else:
			v_f = obj_vel
			v_i = self.velocity
			a = self.scale*((v_f**2)-(v_i**2))/(2.0*d)
		self.velocity += a * dt
		if self.velocity < 0:
			self.velocity = 0
		elif self.velocity > 1:
			self.velocity = 1
		return

	def set_vel(self,vel):
		"""
		set the velocity to be what we specify

		vel - velocity we want

		returns nothing
		"""
		self.velocity = vel

	def slowdown(self,factor):
		"""
		slows down by the given factor
		"""
		self.velocity*=factor

	def get_pos(self):
		"""
		accessor method for the position of the car

		returns the position
		"""
		return self.position

	def get_vel(self):
		"""
		accessor method for the velocity of the car

		returns the velocity
		"""
		return self.velocity

	def add_next(self, next_car):
		"""
		add the next car (car behind our current car). Call this
		when we create a new car. Like this:
		cars.append(car(cars[-1]))
		cars[-2].add_next(cars[-1])

		arguments:
		next_car - the car following our current car

		returns nothing
		"""
		self.next_car = weakref.ref(next_car)

	def remove_prev(self):
		"""
		removes the car we are following from our reference. We don't
		need to call this. When we delete a car, __del__ calls it for us.
		"""
		self.prev_car = None