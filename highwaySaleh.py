from carSaleh import *
import random
import time as count
import csv
import multiprocessing as mp


class simulation(object):

    def __init__(self):
        self.cars = []
        self.num_cars = 0
        self.time = 0.0
        self.dt=.1
        self.highwaylength=1000
        self.delay=0
        self.firstRun=1

        self.printGraph=0

    def run(self, scaling=1, proportion=-1):
        self.scale=scaling
        self.prop=proportion

        while len(self.cars)>0 or self.firstRun:
            self.time += 1

            if not self.firstRun:
               if self.cars[-1].get_vel()<1 and self.cars[-1].get_pos()<=1:
                    self.delay=5
            self.firstRun=0
            if self.delay<=0:
                if self.num_cars <= self.highwaylength:
                    try:
                        self.cars.append(car(self.cars[-1]))
                    except IndexError:
                        self.cars.append(car())
                    self.num_cars += 1
            else:
                self.delay-=1

            self.cars_to_remove=[]
            for k in range(100):
                if 100.0 * random.random() <= 1.0:
                    for m in range(len(self.cars)):
                        if k - self.cars[m].get_pos() <= 1.0 and k - self.cars[m].get_pos() > 0.0:
                            for i in range(10):
                                try:
                                    self.cars[m+i].slowdown(0.5)
                                except:
                                    pass
                            break

            for n in range(10):
                self.num_del = 0
                for i in range(len(self.cars)):
                    if self.cars[i-self.num_del].get_pos() >= 100.0:
                        del self.cars[i-self.num_del]
                        self.num_del += 1
                    else:
                        if self.prop==-1:
                            temp=0
                        if temp < self.prop:
                            #Fast Car
                            self.scale=.56
                            if not self.prop:
                                self.cars[i-self.num_del].update(self.dt, self.scale)
                        else:
                            self.scale=.14
                            self.cars[i-self.num_del].update(self.dt, self.scale)

            if len(self.cars) == 0:
                return self.time
                break

#How you run the class. Will return time as and integer
#sim=simulation()
#print sim.run(1)
#print sim.time
def VaryingAccelConstant(start=0.0, end=6.0, interval=.02, trials=10):
    startTime=count.time()
    results=[]
    for c in range(int(start), int(end/interval)):
        average=[]
        miniStartTime=count.time()
        for t in range(trials):
            #print float(c*interval), t
            average.append(simulation())
            average[-1]=average[-1].run(scaling=float(c)*interval+interval)
        theAverage=0.0
        for item in average:
            theAverage+=item
        results.append([theAverage/10.0, count.time()-miniStartTime])
    print results, count.time()-startTime, "*"*10

    #output to file!

VaryingAccelConstant()

'''
#core is 1-16
output=mp.Queue()
def func(core, output):
    Trials=5
    Results=[]
    SimulationTrial=[]
    startTime=count.time()
    for W in [float(x)*.02 for x in range((core-1)*192/16,core*192/16)]:
        oldTime=count.time()
        factor=1 #not needed
        SimulationTrial.append([])
        for T in range(Trials):
           #print SimulationTrial[-1]
           SimulationTrial[-1].append(simulation())
           SimulationTrial[-1][-1]=SimulationTrial[-1][-1].run(W, 0)

        #Take Average
        #Make Dictionary of Average
        average=0
        print SimulationTrial[-1], "*"
        for single in SimulationTrial[-1]:
            average+=single

        Results.append([W*50, average/Trials])
        print "Iteration:", W*50, "Time:", count.time()-oldTime
        print Results, "*"

    print SimulationTrial
    print "Time it took:", count.time()-startTime

    data = [[data[0], data[1]] for data in Results]
    writer = csv.writer(open("data"+str(core)+".csv", "wb"), dialect="excel")
    print output.get(data)

'''
pass
'''
Trials=3
Results=[]
SimulationTrial=[]
for t in range(10):
    print "Yes"
    oldTime=count.time()
    factor=1 #not needed
    SimulationTrial.append([])
    for T in range(Trials):
       #print SimulationTrial[-1]
       SimulationTrial[-1].append(simulation())
       SimulationTrial[-1][-1]=SimulationTrial[-1][-1].run(1, t*10)
       print "Trial", T

    average=0
    print SimulationTrial[-1], "*"
    for single in SimulationTrial[-1]:
        average+=single

    Results.append([t, average/Trials])
    print "Iteration:", T*50, "Time:", count.time()-oldTime
    print Results, "*"

data = [[data[0], data[1]] for data in Results]
writer = csv.writer(open("data1.csv", "wb"), dialect="excel")
writer.writerows(data)
'''


'''
    for cores in range(1,17):
        print func(cores)
'''