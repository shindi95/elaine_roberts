# Traffic Simulation
A program which models traffic situations where an object (a deer for example) comes out and slows down a car unexpectedly.
We wanted to model the situation where one car rapidly slowing down causes a wave of slow downs to propogate back. This is why traffic can get backed up
for miles just due to one accident.

## Implementation
We made a highway with cars added each time unit (1000 cars over a 100 unit length highway). The first scenario is the cars traveling across the highway 
at constant velocity. The second scenario involves deer being thrown out randomly in the highway. Each time iteration there is a probability (chance=.03
ie 3% chance) that a deer will be thrown out. The program then chooses a random car to get hit with the deer. When a car is hit, it's velocity slows down
by a factor of one half. The acceleration of each car is determined such that the car's position will be equal to the next car in the queue after 1 full 
time units. So if a car is slowed, the car behind it is slowed and if a car is going normal speed, the slowed car will accelerate until it reaches that
speed.

I also wanted to use this program to explore parallel processing. Running this simulation while changing a factor like probability of deer being thrown out,
acceleration, velocity, etc hundreds of times takes a long time. I used Amazon Cloud's EC2 service to run this program much faster than my slow home computer. 

This program was a originally a group project. You can view the oldest commits to see the original group work.
I rewrote it recently to optimize run time and to make the code object oriented.

## Organization
-highway.py		- The main program. It runs the simulation. requires matplotlib
-car.py			- Includes the data structures to run the program. The Car class handles all functions related to individual cars. The Highway class is
a linked list which handles the row of cars on the highway. Note, self.highway[0] is the start of the highway where cars enter and self.highway[-1] is where
cars exit

## Dependencies
python 2.7+ (not 3.x)
matplotlib 1.5+

## Usage
-To run the program so that you can see the plot of the cars, set self.to_plot equal to True (in highway.py, the Simulation class)
and uncomment line 129 and 130 in highway.py
-To run in parallel, uncomment line #126 in highway.py
-To set the number of simulations, change number_of_simulations to whatever number you want on line #103 in highway.py

## Issues
It turns out Python handles parallel processing poorly. As a workaround I used the multiprocessing module but due to an issue with core affinity with
matplotlib, the program would only run on one core despite me using a 32 core instance (c3.8xlarge) with Amazon Cloud Compute. I will revisit this issue
and try to run this program on all cores. As a work around, you can pipe the program at the OS level by running 
[python highway.py | python highway.py | python highway.py| python highway.py | etc] and outputting the data to a file. This is not as cool or elegant 
so I didn't roll with this.

If you have questions, comments, or suggestions please contact shindi@haverford.edu