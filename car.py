# TODO: 3 Unit test each class

'''
Original credit for the Car class is to Randy Doyle. He wrote the original. I reformatted it for my fork to
be a more concrete linked list of a python list of cars. This makes highway.py easier to read and maintain.
'''

class Car(object):
    '''
    A class that makes up each unit (a car) in our modified linked list.
    '''
    def __init__(self):
        self.uniqueID = 0
        self.velocity = 1.0
        self.position = 0.0
        self.min_buffer = 0.01
        self.next = None

    def set_ID(self, newID):
        self.uniqueID = newID

    def get_ID(self):
        return self.uniqueID

    def get_next(self):
        '''
        Accessor function for the next item relative to the current unit
        return: self.next ie the next unit in linked list
        '''
        return self.next

    def set_next(self, newnext):
        '''
        Mutator method that changes the current reference to the next unit to newnext
        param: newnext
        return: Nothing
        '''
        self.next=newnext

    def get_pos(self):
        return self.position

    def get_vel(self):
        return self.velocity

    def set_vel(self, vel):
        self.velocity=vel

    def slowdown(self, factor):
        self.velocity *= factor

    def update(self, dt):
        if self.get_next() != None and self.get_next().get_pos() - self.position <= 1:
            self.accelerate(self.get_next().get_pos(), self.get_next().get_vel(), dt)
        else:
            self.accelerate(self.position + 1.0, 1.0, dt)
        self.position += self.velocity * dt

    def accelerate(self, obj_pos, obj_vel, dt):

        d = obj_pos - self.position - self.min_buffer
        if d == 0:
            a = 0.0
        elif d < 0:
            d *= -1
            v_f = obj_vel if obj_vel < self.velocity else self.velocity
            v_i = obj_vel if obj_vel >= self.velocity else self.velocity
            a = ((v_f ** 2) - (v_i ** 2)) / (2.0 * d)
        else:
            v_f = obj_vel
            v_i = self.velocity
            a = ((v_f ** 2) - (v_i ** 2)) / (2.0 * d)
        self.velocity += a * dt
        if self.velocity < 0:
            self.velocity = 0
        elif self.velocity > 1:
            self.velocity = 1

        return

class Highway(object):
    def __init__(self):
        self.highway=[]
        self.head=None
        self.ID_gen=1

    def __getitem__(self, index):
        return self.highway[index]

    def __str__(self):
        '''

        '''
        string_to_print = ''
        for i in self.highway:
            string_to_print += str(i.get_ID()) + '\n'

        return string_to_print

    def pop_car(self):
        '''
        Called to remove the car at the end (tail) of the highway
        Currently has complexity O(n) but I'm sure there is a more efficient O(1) algorithm

        return: Nothing
        '''
        self.highway.pop()


    def new_car(self):
        '''
        Adds a new car (newcar) to the head of the highway.
        Call when a new car is added to the head of the highway
        parameter: newcar
        return: nothing
        '''
        temp=Car()
        temp.set_ID(self.ID_gen); self.ID_gen += 1
        temp.set_next(self.head)
        self.highway=[temp] + self.highway
        self.head=self.highway[0]

    def size(self):
        '''
        return: number of cars in linked list
        '''
        return len(self.highway)

    def is_empty(self):
        return self.highway == []
