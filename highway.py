# TODO: 2 Comment and document everything
# TODO: 2 Write unit tests
# TODO: 1 Change the name of plott

from car import *
import random
import matplotlib.pyplot as plott
import time as timer
import multiprocessing as multi

class Simulation(object):
    '''
    This is our simulation.
    Cars are added at the head of the highway (position=0) and removed at the tail (position=1000)
    '''
    def __init__(self):
        self.cars = []
        self.num_cars = 0
        self.time = 0
        self.highwaylength=100
        self.dt=.1
        self.max_cars = 1000
        self.delay=0
        self.highway=Highway()
        self.to_plot = False

    def plot(self, plot):
        if plot:
            #insert all plotting functions here
            self.fig=plott.figure()
            self.ax1=self.fig.add_subplot(1, 1, 1)
            plott.ion()

            plott.clf()
            plott.axis([0,100,-1,1])

            xCoord=[]
            for i in range(self.highway.size()):
                xCoord.append(self.highway[i].get_pos()*9)

            plott.plot(xCoord,[.5]*self.highway.size(),"b.")
            print self.highway[-1].get_pos()
            #timer.sleep()

            self.fig.canvas.draw()
            plott.show()

    def add_cars(self):
        '''
        Responsible for adding cars to the head of the highway
        Also responsible for implementing 5 second delay
        '''
        self.time += 1

        if not self.time:
            if self.highway.head.get_vel()<1 and self.highway.head.get_pos()<1:
                self.delay=5

        # If self.delay==0 then keep adding cars. Otherwise pause
        if not self.delay:
            if self.num_cars < self.max_cars:
                self.num_cars += 1
                self.highway.new_car()
        else:
            self.delay -= 1

    def generate_deer(self, chance, slowdownfactor):
        randomnumber = random.random()
        if randomnumber < chance:
            randomcar=random.randint(0, self.highway.size()-1)
            #print self.highway[-1].get_vel(), '||', self.highway.size()
            self.highway[randomcar].slowdown(slowdownfactor)

    def time_iteration(self):
        for i in range (self.highway.size()):
            self.highway[i].update(self.dt)

    def remove_cars(self):
        if self.highway[self.highway.size()-1].get_pos() > self.highwaylength:
            self.highway.pop_car()

    def run(self, plotgraph=False, probability=.03):
        '''
        The main loop for the program.
        Use: after creating a highway object, use object.run()
        return: run time in seconds
        '''
        self.start_time=timer.time()

        while not self.highway.is_empty() > 0 or not self.time:
            self.add_cars()
            #self.generate_deer(chance=probability, slowdownfactor=.5)
            self.time_iteration()
            if not self.highway.is_empty():
                self.remove_cars()
            self.plot(plotgraph)

        self.end_time=timer.time()
        return self.end_time-self.start_time


def multicoreRun2():
    number_of_simulations=128

    TotalSimulation=[0]*number_of_simulations
    TotalSimulation_Times=[0]*number_of_simulations
    output=multi.Queue()

    def main_function():
        output.put(Simulation().run())

    for cores in range(number_of_simulations):
        TotalSimulation[cores]=multi.Process(target=main_function())
        TotalSimulation[cores].start()
        TotalSimulation[cores].join()
        TotalSimulation_Times[cores]=output.get()

    print TotalSimulation_Times
    avg=0
    for i in TotalSimulation_Times:
        avg += i
    return float(avg)/float(len(TotalSimulation_Times))

if __name__ ==  '__main__':
    #Run the below line to run this simulation 128 times on as many cores as the machine has
    multicoreRun2()

    #Run the next two lines to watch the simulation take place graphically
    #s=Simulation()
    #s.run(plotgraph=True)